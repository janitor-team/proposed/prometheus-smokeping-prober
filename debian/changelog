prometheus-smokeping-prober (0.5.2-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.6.1 (no changes)
  * Update flag defaults in debian/default
  * Add missing build-deps
    - golang-golang-x-sync-dev
    - golang-gopkg-yaml.v2-dev

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sat, 14 May 2022 15:21:08 +0000

prometheus-smokeping-prober (0.5.0-1) unstable; urgency=medium

  * New upstream version
  * Drop obsolete 01-go-kit-log.patch
  * Add new 01-debian-defaults.patch
  * Convert XS-Go-Import-Path to lower case to match that of go.mod, and avoid
    case-insensitive import error
  * Update flags in debian/default
  * Install default config file in /etc/prometheus

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sun, 19 Dec 2021 15:08:39 +0000

prometheus-smokeping-prober (0.4.2-3) unstable; urgency=medium

  * Rebuild against newer go-ping/ping module (Closes: #998231)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Fri, 17 Dec 2021 13:39:31 +0000

prometheus-smokeping-prober (0.4.2-2) unstable; urgency=medium

  * Enable CAP_NET_RAW for prometheus-smokeping-prober binary by default when
    installing (Closes: #990715)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sun, 05 Dec 2021 17:41:59 +0000

prometheus-smokeping-prober (0.4.2-1) unstable; urgency=medium

  [ Adriano Rafael Gomes ]
  * Initial Brazilian Portuguese debconf translation (Closes: #987433)

  [ Aloïs Micard ]
  * Update debian/gitlab-ci.yml

  [ Américo Monteiro ]
  * Initial Portuguese debconf translation (Closes: #982811)

  [ Camaleón ]
  * Initial Spanish debconf translation (Closes: #988381)

  [ Daniel Swarbrick ]
  * New upstream release
  * Bump Standards-Version to 4.6.0 (no changes)
  * Add new patch 01-go-kit-log.patch to build with newer version of
    prometheus/common
  * Use dh-sequence-golang instead of dh-golang and --with=golang
  * Update copyright legal entity name

  [ Frans Spiesschaert ]
  * Initial Dutch debconf translation (Closes: #983413)

  [ Helge Kreutzmann ]
  * Initial German debconf translation (Closes: #982629)

  [ Jean-Pierre Giraud ]
  * Initial French debconf translation (Closes: #983856)

 -- Daniel Swarbrick <dswarbrick@debian.org>  Sat, 04 Dec 2021 19:18:27 +0000

prometheus-smokeping-prober (0.4.1-2) unstable; urgency=medium

  * Team upload.
  * No change upload to rebuild on buildd.

 -- Benjamin Drung <bdrung@debian.org>  Sun, 07 Feb 2021 16:17:53 +0100

prometheus-smokeping-prober (0.4.1-1) unstable; urgency=medium

  * Initial release (Closes: #981741).

 -- Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>  Wed, 03 Feb 2021 14:49:37 +0100
